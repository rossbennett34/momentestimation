#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

using namespace Rcpp;

// [[Rcpp::export]]
arma::mat M2_Rcpp(const arma::mat& x) {
  // get the dimensions
  unsigned int n = x.n_rows;
  unsigned int p = x.n_cols;
  
  // allocate matrix
  arma::mat m2_mat(p, p, arma::fill::zeros);
  
  for(int i = 0; i < n; i++){
    // tmp is a 1 x p matrix where p is the number of columns
    arma::rowvec tmp = x.row(i);
    m2_mat = m2_mat + trans(tmp) * tmp;
  }
  return m2_mat * 1 / static_cast<double>(n);
}

// [[Rcpp::export]]
arma::mat M3_Rcpp(const arma::mat& x) {
  // get the dimensions
  unsigned int n = x.n_rows;
  unsigned int p = x.n_cols;
  
  // allocate matrix
  arma::mat m3_mat(p, p*p, arma::fill::zeros);
  
  for(int i = 0; i < n; i++){
    // tmp is a 1 x p matrix where p is the number of columns
    // this is different how R extracts rows of matrices
    arma::rowvec tmp = x.row(i);
    m3_mat = m3_mat + arma::kron(trans(tmp) * tmp, tmp);
  }
  return m3_mat * 1 / static_cast<double>(n);
}

// [[Rcpp::export]]
arma::mat M4_Rcpp(const arma::mat& x) {
  // get the dimensions
  unsigned int n = x.n_rows;
  unsigned int p = x.n_cols;
  
  // allocate matrix
  arma::mat m4_mat(p, p*p*p, arma::fill::zeros);
  
  for(int i = 0; i < n; i++){
    // tmp is a 1 x p matrix where p is the number of columns
    // this is different how R extracts rows of matrices
    arma::rowvec tmp = x.row(i);
    m4_mat = m4_mat + arma::kron(trans(tmp) * tmp, kron(tmp, tmp));
  }
  return m4_mat * 1 / static_cast<double>(n);
}

/*
Extract columns and rows from a matrix
arma::mat X(n_rows, n_cols, arma::fill::zeros)
arma::colvec X.col( col_number )
arma::rowvec X.row( row_number )
*/