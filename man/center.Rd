\name{center}
\alias{center}
\title{Center}
\usage{
  center(x)
}
\arguments{
  \item{x}{xts or matrix}
}
\value{
  centered data
}
\description{
  Center a matrix or time series object
}
\details{
  This function is used primarily to center a time series
  of asset returns or factors. Each column should represent
  the returns of an asset or factor realization. The
  expected value is taken as the sample mean.

  x.centered = x - mean(x)
}

