
# General sample estimates for 2nd, 3rd, and 4th moments for multivariate data
M2 <- function(x){
  # x should be centered before passing to this function
  
  # Dimensions
  n <- nrow(x)
  p <- ncol(x)
  
  # Allocate matrix
  M2 <- matrix(0, nrow=p, ncol=p)
  
  for(t in c(1:n)){
    M2 <- M2 + tcrossprod(x[t,])
  }
  out <- (1 / n) * M2
  out
}


M2.MM <- function(x){
  # x should be centered before passing to this function
  # Do some checks on x
  if(!is.matrix(x)) stop("x must be a matrix")
  
  # Call compiled function to estimate 2nd moment
  out <- M2_Rcpp(x)
  return(out)
}


M3 <- function(x){
  # x should be centered before passing to this function
  
  # Dimensions
  n <- nrow(x)
  p <- ncol(x)
  
  # Allocate matrix
  M3 <- matrix(0, nrow=p, ncol=p^2)
  
  for(t in c(1:n)){
    M3 <- M3 + tcrossprod(x[t,]) %x% t(x[t,])
  }
  out <- (1 / n) * M3
  out
}

M3.MM <- function(x){
  # x should be centered before passing to this function
  # Do some checks on x
  if(!is.matrix(x)) stop("x must be a matrix")
  
  # Call compiled function to estimate 3rd moment
  out <- M3_Rcpp(x)
  return(out)
}

M4 <- function(x){
  # x should be centered before passing to this function
  
  # Dimensions
  n <- nrow(x)
  p <- ncol(x)
  
  # Allocate matrix
  M4 <- matrix(0, nrow=p, ncol=p^3)
  
  for(t in c(1:n)){
    M4 <- M4 + tcrossprod(x[t,]) %x% t(x[t,]) %x% t(x[t,])
  }
  out <- (1 / n) * M4
  out
}

M4.MM <- function(x){
  # x should be centered before passing to this function
  # Do some checks on x
  if(!is.matrix(x)) stop("x must be a matrix")
  
  # Call compiled function to estimate 4th moment
  out <- M4_Rcpp(x)
  return(out)
}
